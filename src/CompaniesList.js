import React, {Component} from "react";
import Company from "./Company";

class CompaniesList extends Component {
  constructor(props) {
    super(props);
    this.state = {companies: []};

    this.fetchCompanies = this.fetchCompanies.bind(this);
    this.onTestButtonClick = this.onTestButtonClick.bind(this);
  }

  componentDidMount() {
    this.fetchCompanies();
  }

  fetchCompanies() {
    fetch("http://localhost:8080/companies")
      .then(response => response.json())
      .then(companies => {
        this.setState({companies: companies});
      });
  }

  onTestButtonClick(companyId) {
    fetch("http://localhost:8080/company/" + companyId)
      .then(response => response.json())
      .then(company => alert("Ettevõtte nimi: " + company.name));
  }

  render() {
    console.log("Render meetod käivitus");
    if (this.state.companies.length > 0) {
      return (
        <div>
          Ettevõtete arv: {this.state.companies.length}
          <br />
          {this.state.companies.map(company => {
            return (
              <Company
                key={company.id}
                companyToDisplay={company}
                onButtonClick={this.onTestButtonClick}
              />
            );
          })}
        </div>
      );
    } else {
      return <div>Ettevõtteid ei ole.</div>;
    }
  }
}

export default CompaniesList;
