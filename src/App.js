import React, {Component} from "react";
import "./App.css";
import CompaniesList from "./CompaniesList";

class App extends Component {
  render() {
    return (
      <div className="App">
        Test
        <br />
        <CompaniesList />
      </div>
    );
  }
}

export default App;
