import React, {Component} from "react";
import {Button} from "reactstrap";

class Company extends Component {
  render() {
    return (
      <div key={this.props.companyToDisplay.id}>
        {this.props.companyToDisplay.name}
        <br />
        <img height="50" src={this.props.companyToDisplay.logo} alt="logo" />
        <br />
        <Button
          onClick={() =>
            this.props.onButtonClick(this.props.companyToDisplay.id)
          }
        >
          Sample Button
        </Button>
        <br />
        <br />
      </div>
    );
  }
}

export default Company;
